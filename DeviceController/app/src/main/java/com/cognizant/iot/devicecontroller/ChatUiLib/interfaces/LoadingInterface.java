package com.cognizant.iot.devicecontroller.ChatUiLib.interfaces;

/**
 * Created by Bharath on 03/05/17.
 */

public interface LoadingInterface {
    void hideLoading();
    void removeLastItem(int position);
}
