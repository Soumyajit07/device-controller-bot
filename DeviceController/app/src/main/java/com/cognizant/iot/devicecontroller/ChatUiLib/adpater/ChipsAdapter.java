package com.cognizant.iot.devicecontroller.ChatUiLib.adpater;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.devicecontroller.MainActivity;
import com.cognizant.iot.devicecontroller.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bharath on 25/04/17.
 */

public class ChipsAdapter extends RecyclerView.Adapter<ChipsAdapter.MyViewHolder> {

    private List<String> chipList;
    MainActivity mainActivity;
    ChatAdapter chatAdapter;
    int position;


    public ChipsAdapter(ArrayList<String> chipsList, MainActivity mainActivity, int position, ChatAdapter chatAdapter) {
        this.chipList = chipsList;
        this.mainActivity = mainActivity;
        this.position = position;
        this.chatAdapter = chatAdapter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView chipTextView;

        public MyViewHolder(View v) {
            super(v);
            chipTextView = (TextView) v.findViewById(R.id.chip_text);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.onChipClicked(chipList.get(getAdapterPosition()));
                    chatAdapter.removeLastItem(position);
                }
            });
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chip_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.chipTextView.setText(chipList.get(position));
    }

    @Override
    public int getItemCount() {
        return chipList.size();
    }
}
