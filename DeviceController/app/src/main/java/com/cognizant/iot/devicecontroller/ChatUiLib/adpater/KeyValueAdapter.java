package com.cognizant.iot.devicecontroller.ChatUiLib.adpater;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.devicecontroller.R;

import java.util.HashMap;

/**
 * Created by Bharath on 26/04/17.
 */

public class KeyValueAdapter extends RecyclerView.Adapter<KeyValueAdapter.MainViewHolder> {
    LayoutInflater inflater;
    HashMap<String, String> modelList;

    public KeyValueAdapter(Context context, HashMap<String, String> modelList) {
        this.inflater = LayoutInflater.from(context);
        this.modelList = modelList;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.key_value_item, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        for (int i = 0; i < modelList.size(); i++) {
            if (i == position) {

                Object key = modelList.keySet().toArray()[position].toString();
                Object valueForKey = modelList.get(key);
                holder.mainText.setText(key.toString());
                holder.subText.setText(valueForKey.toString());
            }
        }

    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    class MainViewHolder extends RecyclerView.ViewHolder {

        TextView mainText, subText;

        public MainViewHolder(View itemView) {
            super(itemView);
            mainText = (TextView) itemView.findViewById(R.id.mainText);
            subText = (TextView) itemView.findViewById(R.id.subText);
        }

    }

}