package com.cognizant.iot.devicecontroller.ChatUiLib.adpater;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.devicecontroller.ChatUiLib.ChatUI;
import com.cognizant.iot.devicecontroller.ChatUiLib.Utils;
import com.cognizant.iot.devicecontroller.ChatUiLib.interfaces.LoadingInterface;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.KeyValueModel;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.TextModel;
import com.cognizant.iot.devicecontroller.MainActivity;
import com.cognizant.iot.devicecontroller.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Bharath on 24/04/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements LoadingInterface {

    ArrayList<Object> chatDatasList;
    ArrayList<Integer> elementType;
    Context context;
    MainActivity mainActivity;
    ChatUI chatUI;

    @Override
    public void removeLastItem(int position) {
        chatDatasList.remove(position);
        elementType.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, chatDatasList.size());
        chatUI.updateChatUI();
    }

    @Override
    public void hideLoading() {

        for (int i = 0; i < elementType.size(); i++) {
            if (elementType.get(i) == Utils.SHOW_LOADING) {
                chatDatasList.remove(i);
                elementType.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, chatDatasList.size());
                chatUI.updateChatUI();
            }
        }
    }

    public class TextHolder extends RecyclerView.ViewHolder {
        public TextView text, timeStamp;

        public TextHolder(View view) {
            super(view);
            text = (TextView) view.findViewById(R.id.text_view);
            timeStamp = (TextView) view.findViewById(R.id.time_stamp);
        }
    }

    public class LoadingHolder extends RecyclerView.ViewHolder {
        AVLoadingIndicatorView indicatorView;

        public LoadingHolder(View view) {
            super(view);

            indicatorView = (AVLoadingIndicatorView) view.findViewById(R.id.avi);
        }
    }

    public class HorizontalListHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;

        public HorizontalListHolder(View v) {
            super(v);
            recyclerView = (RecyclerView) v.findViewById(R.id.horizontal_recyc);

        }
    }

    public class KeyValueListHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView title;

        public KeyValueListHolder(View v) {
            super(v);
            recyclerView = (RecyclerView) v.findViewById(R.id.vertical_recyc);
            title = (TextView) v.findViewById(R.id.title_text_view);

        }
    }


    public ChatAdapter(ArrayList<Object> chatDatasList, ArrayList<Integer> elementType, Context context, ChatUI chatUI, MainActivity mainActivity) {
        this.chatDatasList = chatDatasList;
        this.elementType = elementType;
        this.context = context;
        this.chatUI = chatUI;
        this.mainActivity = mainActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View sendView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_send, parent, false);

        View receiveView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_receive, parent, false);

        View listView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_recyc, parent, false);

        View vertical_listView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_recyc_vertical, parent, false);

        View loadingView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.loader, parent, false);

        switch (viewType) {
            case Utils.SEND:
                return new TextHolder(sendView);
            case Utils.RECEIVE:
                return new TextHolder(receiveView);
            case Utils.RECEIVE_PRODUCT:
                return new HorizontalListHolder(listView);
            case Utils.RECEIVE_OFFER_LIST:
                return new HorizontalListHolder(listView);
            case Utils.RECEIVE_SEARCH_LIST:
                return new HorizontalListHolder(listView);
            case Utils.CHIPS_LIST:
                return new HorizontalListHolder(listView);
            case Utils.KEY_VALUE_PAIR:
                return new KeyValueListHolder(vertical_listView);
            case Utils.SHOW_LOADING:
                return new LoadingHolder(loadingView);
            default:
                return new TextHolder(null);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {
            case Utils.SEND:
                TextHolder holder = (TextHolder) viewHolder;
                TextModel chatData = (TextModel) chatDatasList.get(position);
                holder.text.setText(chatData.getText());
                holder.timeStamp.setText(chatData.getTime());
                break;
            case Utils.RECEIVE:
                TextHolder holderReceive = (TextHolder) viewHolder;
                TextModel chatDataReceive = (TextModel) chatDatasList.get(position);
                holderReceive.text.setText(chatDataReceive.getText());
                holderReceive.timeStamp.setText(chatDataReceive.getTime());
                break;
            case Utils.RECEIVE_PRODUCT:
                HorizontalListHolder horizontalListHolder = (HorizontalListHolder) viewHolder;
                ArrayList<DataModel> productModels = new ArrayList<>((ArrayList<DataModel>) chatDatasList.get(position));
                ProductAdapter mAdapter = new ProductAdapter(productModels, mainActivity);
                horizontalListHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                horizontalListHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                horizontalListHolder.recyclerView.setAdapter(mAdapter);
                break;
            case Utils.RECEIVE_OFFER_LIST:
                HorizontalListHolder offferListHolder = (HorizontalListHolder) viewHolder;
                ArrayList<DataModel> offerModels = new ArrayList<>((ArrayList<DataModel>) chatDatasList.get(position));
                ListAdapter offerAdapter = new ListAdapter(offerModels, mainActivity, elementType.get(position));
                offferListHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                offferListHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                offferListHolder.recyclerView.setAdapter(offerAdapter);
                break;
            case Utils.RECEIVE_SEARCH_LIST:
                HorizontalListHolder searchListHolder = (HorizontalListHolder) viewHolder;
                ArrayList<DataModel> searchModels = new ArrayList<>((ArrayList<DataModel>) chatDatasList.get(position));
                ListAdapter searchAdapter = new ListAdapter(searchModels, mainActivity, elementType.get(position));
                searchListHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                searchListHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                searchListHolder.recyclerView.setAdapter(searchAdapter);
                break;
            case Utils.CHIPS_LIST:
                HorizontalListHolder chipsListHolder = (HorizontalListHolder) viewHolder;
                ArrayList<String> chipsList = new ArrayList<>((ArrayList<String>) chatDatasList.get(position));
                ChipsAdapter chipsAdapter = new ChipsAdapter(chipsList, mainActivity, position, this);
                chipsListHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                chipsListHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                chipsListHolder.recyclerView.setAdapter(chipsAdapter);
                break;
            case Utils.KEY_VALUE_PAIR:
                KeyValueListHolder keyValueListHolder = (KeyValueListHolder) viewHolder;
                KeyValueModel keyValueModel = (KeyValueModel) chatDatasList.get(position);
                keyValueListHolder.title.setText(keyValueModel.getTitle());
                HashMap<String, String> mapList = new HashMap<>(keyValueModel.getStringHashMap());
                KeyValueAdapter keyValueAdapter = new KeyValueAdapter(mainActivity, mapList);
                keyValueListHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                keyValueListHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                keyValueListHolder.recyclerView.setAdapter(keyValueAdapter);
                break;
            case Utils.SHOW_LOADING:
                LoadingHolder loadingHolder = (LoadingHolder) viewHolder;
                loadingHolder.indicatorView.show();
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        return elementType.get(position);
    }

    @Override
    public int getItemCount() {
        return chatDatasList.size();
    }
}