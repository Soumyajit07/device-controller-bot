package com.cognizant.iot.devicecontroller.ChatUiLib.model;

import java.util.HashMap;

/**
 * Created by Bharath on 26/04/17.
 */

public class KeyValueModel {
    String title;
    HashMap<String, String> stringHashMap;

    public KeyValueModel(String title, HashMap<String, String> stringHashMap) {
        this.title = title;
        this.stringHashMap = stringHashMap;
    }

    public String getTitle() {
        return title;
    }

    public HashMap<String, String> getStringHashMap() {
        return stringHashMap;
    }
}
