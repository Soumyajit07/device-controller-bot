package com.cognizant.iot.devicecontroller.ChatAsyncTask;

import android.os.AsyncTask;
import android.util.Log;


import com.cognizant.iot.devicecontroller.ChatUiLib.ChatUI;
import com.cognizant.iot.devicecontroller.UbidotsTask.ApiUbidots;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

/**
 * Created by 540472 on 5/24/2017.
 * This Class sends user queries to API.AI and handles intent in switch case.
 */
public class ApiAiTask extends AsyncTask<String, Void, AIResponse> {
    private AIError aiError;
    ChatUI chatUI;
    AIDataService aiDataService;

    public ApiAiTask(ChatUI chatUI, AIDataService aiDataService) {
        this.chatUI = chatUI;
        this.aiDataService = aiDataService;
    }

    @Override
    protected AIResponse doInBackground(final String... params) {
        final AIRequest request = new AIRequest();
        String query = params[0];
        request.setQuery(query);

        try {
            final AIResponse response = aiDataService.request(request);
            return response;
        } catch (final AIServiceException e) {
            aiError = new AIError(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(final AIResponse response) {
        if (response != null) {
            String intent=response.getResult().getMetadata().getIntentName().toString().toLowerCase();
            boolean actionIncomplete = response.getResult().isActionIncomplete();
            String speech = response.getResult().getFulfillment().getSpeech();
            Log.e("TAG", "actionIncomplete = " + actionIncomplete
                    + "\nintent = " + intent
                    + "\nspeech = " + speech);
            switch (intent){

                case "greetings" :
                    chatUI.showReceiveMessage(speech);
                    break;
                case "bye" :
                    chatUI.showReceiveMessage(speech);
                    break;
                case "turn_on_light" :
                    Log.e("in","turn_on_light");
                    chatUI.showReceiveMessage(speech);
                    new ApiUbidots().execute(1);
                    break;
                case "turn_off_light" :
                    Log.e("in","turn_off_light");
                    chatUI.showReceiveMessage(speech);
                    new ApiUbidots().execute(0);
                    break;
                case "default fallback intent" :
                    break;
            }

        } else {
            Log.e("TAG","API.AI Error : "+aiError.getMessage().toString());
            chatUI.showReceiveMessage("I'm unable to reach my server please try later.");
        }
    }
}
