package com.cognizant.iot.devicecontroller.ChatUiLib.adpater;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.cognizant.iot.devicecontroller.ChatUiLib.Utils;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;
import com.cognizant.iot.devicecontroller.MainActivity;
import com.cognizant.iot.devicecontroller.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Bharath on 25/04/17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private List<DataModel> modelsList;
    MainActivity mainActivity;
    Integer type;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView headingTextView, contentTextView;

        public MyViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image_view);
            headingTextView = (TextView) v.findViewById(R.id.text_heading);
            if (type == Utils.RECEIVE_SEARCH_LIST) {
                contentTextView = (TextView) v.findViewById(R.id.text_content);
            }
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type == Utils.RECEIVE_SEARCH_LIST) {
                        mainActivity.onSearchClicked(modelsList.get(getPosition()));
                    } else if (type == Utils.RECEIVE_OFFER_LIST) {
                        mainActivity.onOfferClicked(modelsList.get(getPosition()));
                    }
                }
            });
        }
    }


    public ListAdapter(List<DataModel> modelsList, MainActivity mainActivity, Integer type) {
        this.modelsList = modelsList;
        this.mainActivity = mainActivity;
        this.type = type;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (type == Utils.RECEIVE_SEARCH_LIST) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_info, parent, false);
        } else if (type == Utils.RECEIVE_OFFER_LIST) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_offer_image, parent, false);
        } else {
            itemView = null;
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataModel productModel = modelsList.get(position);
        holder.headingTextView.setText(productModel.getText());

        Log.e("@@##", type + " asf");
        if (type == Utils.RECEIVE_SEARCH_LIST) {
            holder.contentTextView.setText(productModel.getInfo());
        }

        if (productModel.getImageURL() == null)
            holder.imageView.setImageResource(productModel.getImageResourceId());
        else
            Picasso.with(mainActivity).load(productModel.getImageURL()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return modelsList.size();
    }
}
