package com.cognizant.iot.devicecontroller.ChatUiLib.adpater;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;
import com.cognizant.iot.devicecontroller.MainActivity;
import com.cognizant.iot.devicecontroller.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Bharath on 25/04/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<DataModel> productModelsList;
    MainActivity mainActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView productImage;
        TextView productName, productPrice;

        public MyViewHolder(View v) {
            super(v);
            productImage = (ImageView) v.findViewById(R.id.product_image);
            productName = (TextView) v.findViewById(R.id.product_name);
            productPrice = (TextView) v.findViewById(R.id.product_price);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.onProductClicked(productModelsList.get(getPosition()));
                }
            });
        }
    }


    public ProductAdapter(List<DataModel> ProductModelsList, MainActivity mainActivity) {
        this.productModelsList = ProductModelsList;
        this.mainActivity = mainActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_product, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataModel productModel = productModelsList.get(position);
        holder.productName.setText(productModel.getText());
        holder.productPrice.setText(productModel.getInfo());
        if (productModel.getImageURL()== null)
            holder.productImage.setImageResource(productModel.getImageResourceId());
        else
            Picasso.with(mainActivity).load(productModel.getImageURL()).into(holder.productImage);
    }

    @Override
    public int getItemCount() {
        return productModelsList.size();
    }
}
